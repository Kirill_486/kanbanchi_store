import {chargeBeeProds, monthlyPlans, billingWordingOptions} from '../constants';
import { IProduct, IApplicationState } from '../../types/types';
import {ISelectOption} from '../../types/components';
import {products} from '../defaults';
import store from '../configureStore';
import moment = require('moment');

// todo return live data
export const getProducts = () => {
    return products;
}

export const getProductById = (id: number): IProduct => {
    const products = getProducts();
    const result = products.find((item) => item.id === id);
    return result;
};

// todo rewrite
export const getBillingTypeById = (productId: number) => {
    //const product = getProductById(productId);
    const isChargeBeeProd = chargeBeeProds.includes(productId);
    const isMonthly = monthlyPlans.includes(productId);
    if (!isChargeBeeProd) {
        return 'not a bee'
    } else {
        return isMonthly ? 'monthly' : 'annual';
    }
};

export const getBillingWording = () => {
    const appState = store.getState() as IApplicationState;
    const currentSubscription = appState.subscription.subscription;
    const isSubscriptionActive = currentSubscription.status === 'Active';
    const isSubscriptionExpired = currentSubscription.paymentExpDate < Date.now() / 1000;

    if (isSubscriptionActive && !isSubscriptionExpired) {
        return billingWordingOptions.newxtBilling;
    } else if (!isSubscriptionExpired) {
        return billingWordingOptions.expirationDate;
    } else {
        return billingWordingOptions.expired;
    }
}

export const getDateHumanReadable = (expireDate: number): string => {
    return moment.unix(expireDate).format('DD MMM YYYY');
}

export const getSubscriptionIdByProductId = (productId: number): number => {
    const appState = store.getState() as IApplicationState;
    const availableSubscriptions = appState.subscription.subscriptions;
    const subscriptionByProductId = availableSubscriptions.find(item => item.productId === productId);
    return subscriptionByProductId.id;
}

export const selectAvailableProducts = (): IProduct[] => {
    const appState = store.getState() as IApplicationState;
    const products = getProducts();
    const subscriptions = appState.subscription.subscriptions;
    const availableProducts = subscriptions.map((item) => {
        const productId = item.productId;
        return products.find(item => item.id === productId);
    });
    return availableProducts;
};

export const getProductSelectOptions = () => {
    const availableProducts = selectAvailableProducts();
    const selectOptions = availableProducts.map((item): ISelectOption => {
        return {
            value: item.id,
            label: item.name
        }
    });
    return selectOptions;
};

export const selectCurrentOption = () => {
    let currentOption;
    const appState = store.getState() as IApplicationState;
    const currentSubscription = appState.subscription.subscription;
    const availableProductsOptions = getProductSelectOptions();
    if (currentSubscription) {
        currentOption = currentSubscription.productId;
    } else {
        const firstOption = availableProductsOptions[0];
        if (firstOption) {
            currentOption = firstOption.value;
        } else {
            currentOption = 0;
        }
    }

    return currentOption;
}
