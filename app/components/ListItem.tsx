import * as React from 'react';
import SVG from './SVG';
import { IListItemProps } from '../types/components';

export const ListItem  = (props: IListItemProps) => {
    return (
        <div
            className="subscription-item__container"
            onDoubleClick={props.startEdit}
        >
            <div className="subscription-item__user-details">
                <span
                    className="
                        icon
                        icon--g
                        subscription-item__user-avatar">
                    <SVG xlink="svgicon--user"/>
                </span>
                <span className="subscription-item__text">
                    {props.email}
                </span>
            </div>

            { props.areButtonsShowing &&
                (
                    <div className="subscription-item__button-panel">
                        <span
                            onClick={props.startDelete}
                            className="icon icon--g subscription-item__button">
                            <SVG xlink="svgicon--delete_admin" />
                        </span>
                        <span
                            onClick={props.startEdit}
                            className="icon icon--g subscription-item__button">
                            <SVG xlink="svgicon--edit" />
                        </span>
                    </div>
                )
            }
        </div>
    );
};
