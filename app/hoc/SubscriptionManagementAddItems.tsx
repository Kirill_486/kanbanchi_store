import { IApplicationState } from '../types/types';
import { IAddItemsProps, IAddItemsStateProps, IAddItemsDispatchProps } from '../types/components';
import { connect } from 'react-redux';

import {AddItems} from '../components/AddItems';

import {openInput, closeInput, changeInput} from '../store/actions/subscriptionManagementActions';
import { asyncAddItemsAction } from '../store/actions/subscriptionActions';
import { availableItemsSelector } from '../store/selectors/itemsSelector';

const mapStateToProps = (state: IApplicationState): IAddItemsStateProps => {
    const isAddItemsShown =
        !state.subscriptionManagement.isItemEditing
        && !state.subscriptionManagement.searchingString
        && !state.subscriptionManagement.isSearchOpen;
    return {
        isShown: isAddItemsShown,
        inputValue: state.subscriptionManagement.inputString,
        isInputOpen: state.subscriptionManagement.inputOpen,
        recognizedItems: state.subscriptionManagement.recognizedItems,
        seatsAvailable: availableItemsSelector(state).length
    }
}

const mapDispatchToProps = (dispatch: any, props: IAddItemsProps): IAddItemsDispatchProps => {
    return {
        addMembers: () => {
            dispatch(asyncAddItemsAction())
        },
        closeInput: () => dispatch(closeInput()),
        openInput: () => dispatch(openInput()),
        inputChamge: (e: any) => {
            const newInputValue = e.target.value as string;
            dispatch(changeInput(newInputValue));
        }
    }
};

const connectedAddItems = connect(mapStateToProps, mapDispatchToProps)(AddItems);
connectedAddItems.displayName = 'PaymentAddItems';
export default connectedAddItems;
