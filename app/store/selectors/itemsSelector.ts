import { IApplicationState, ISubscriptionItem } from '../../types/types';
import { isItemInSearchResults } from '../helpers/subscriptionHelper';

export const selectItems = (state: IApplicationState): ISubscriptionItem[] => {
    const areAnyItems =
        state.subscription.items &&
        state.subscription.items.length > 0;
    let usedItems: ISubscriptionItem[];
    if (areAnyItems) {
        usedItems = state.subscription.items.filter((item) => item.email);
        const searchingString = state.subscriptionManagement.searchingString;
        const isSearch = !!searchingString;

        if (isSearch) usedItems = usedItems.filter(
            (item) => isItemInSearchResults(item, searchingString)
        );
    } else {
        usedItems = [];
    }
    return usedItems;
}

export const availableItemsSelector = (state: IApplicationState): ISubscriptionItem[] => {
    const items = state.subscription.items;
    const availableItems = items.filter(item => !item.email);
    return availableItems;
}

export const usedItemsSelector = (state: IApplicationState): ISubscriptionItem[] => {
    const items = state.subscription.items;
    const usedItems = items.filter(item => !!item.email);
    return usedItems;
}
