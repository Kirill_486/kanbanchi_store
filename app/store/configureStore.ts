import { combineReducers, createStore } from 'redux';
import { applyMiddleware } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import subscriptionReducer from './reducers/subscriptionReducer';
import SubscriptionManagementReducer from './reducers/subscriptionManagementReducer'
import messageToUserReducer from './reducers/messageToUserReducer';
import { Store } from 'react-redux';
import { IApplicationState } from '../types/types';

const store: Store<IApplicationState> = createStore(combineReducers({
    subscription: subscriptionReducer,
    subscriptionManagement: SubscriptionManagementReducer,
    messageToUser: messageToUserReducer
}), composeWithDevTools(applyMiddleware(thunk)));

export default store;
