import { IApplicationState } from '../../types/types';
import {subscriptionManagementActionTypes} from '../constants';
import { Action } from 'redux';
import { ActionCreator } from 'react-redux';
import store from '../configureStore';
import { asyncEditItemAction } from './subscriptionActions';
import { ISubscriptionManagementTextAction, ISubscriptionManagementIdAction, IUserMessageAction } from '../../types/actions';

export const openInput: ActionCreator<Action> = () => {
    return {
        type: subscriptionManagementActionTypes.OPEN_INPUT
    }
};

export const closeInput: ActionCreator<Action> = () => {
    return {
        type: subscriptionManagementActionTypes.CLOSE_INPUT
    }
};

export const changeInput: ActionCreator<ISubscriptionManagementTextAction> =
(payload?: string) => {
    return {
        type: subscriptionManagementActionTypes.CHANGE_INPUT,
        payload
    }
};

export const clearInput: ActionCreator<Action> =
() => {
    return {
        type: subscriptionManagementActionTypes.CLEAR_INPUT
    }
};

export const openSearch: ActionCreator<Action> =
() => {
    return {
        type: subscriptionManagementActionTypes.OPEN_SEARCH
    }
};

export const closeSearch: ActionCreator<Action> =
() => {
    return {
        type: subscriptionManagementActionTypes.CLOSE_SEARCH
    }
};

export const changeSearch: ActionCreator<ISubscriptionManagementTextAction> =
(payload: string) => {
    return {
        type: subscriptionManagementActionTypes.CHANGE_SEARCH,
        payload
    }
};
export const clearSearch: ActionCreator<Action> =
() => {
    return {
        type: subscriptionManagementActionTypes.CLEAR_SEARCH
    }
};

export const openEditing: ActionCreator<ISubscriptionManagementIdAction> =
(id: number) => {
    return {
        type: subscriptionManagementActionTypes.OPEN_EDITING,
        payload: id
    }
};

export const closeEditing: ActionCreator<Action> =
() => {
    return {
        type: subscriptionManagementActionTypes.CLOSE_EDITING
    }
};

export const changeEditing: ActionCreator<ISubscriptionManagementTextAction> =
(newValue: string) => {
    return {
        type: subscriptionManagementActionTypes.CHANGE_EDITING,
        payload: newValue
    }
};

export const applyEditing: ActionCreator<any> =
() => {
    const appState = store.getState() as IApplicationState;
    const subscriptionId = appState.subscription.subscription.id;
    const editingItemId = appState.subscriptionManagement.editingItemId;
    const editedEmail = appState.subscriptionManagement.editingValue;
    const items = appState.subscription.items.map((item) => {
        if (item.id === editingItemId) {
            item = {
                ...item,
                email: editedEmail
            }
        }
        return item;
    });
    const action = (dispatch: any) => {
        dispatch(asyncEditItemAction())
    }
    return action;
};

export const resetState: ActionCreator<any> = () => {

    const action = (dispatch: any) => {
        dispatch(closeInput());
        dispatch(clearInput());

        dispatch(closeEditing());

        dispatch(closeSearch());
    };

    return action;
};

export const showDoneCansel: ActionCreator<IUserMessageAction> = (
    id: number,
    confirmAction: any,
    denyAction: any
) => {
    return {
        type: subscriptionManagementActionTypes.SHOW_DONE_CANCEL,
        payload: {
            id,
            confirmAction,
            denyAction
        }
    }
};

export const hideDoneCancel: ActionCreator<Action> = () => {
    return {
        type: subscriptionManagementActionTypes.HIDE_DONE_CANCEL
    };
};

export const confirmAction: ActionCreator<Action> = () => {
    return {
        type: subscriptionManagementActionTypes.CONFIRM_ACTION
    }
};
