import { IApplicationState } from '../types/types';
import store from '../store/configureStore';
import { Baloon } from '../components/Baloon';
import {connect} from 'react-redux'
import { IMessageToUserStateProps, IMessageToUserDispatchProps } from '../types/components';

const mapStateToProps = (
    state: IApplicationState
    ): IMessageToUserStateProps => {
    return {
        isShown: state.messageToUser.isShown,
        message: state.messageToUser.message
    }
}

const mapDispatchToProps = (
    dispatch: any
    ): IMessageToUserDispatchProps => {
    return {
        onConfirm: () => {
            const appState = (store.getState() as IApplicationState);
            const confirmAction = appState.messageToUser.confirmAction;
            if (confirmAction) {
                dispatch(confirmAction);
            }else {
                console.warn('Confirmation action is not set');
            }

        } ,
        onDeny: () => {
            const appState = (store.getState() as IApplicationState);
            const denyAction = appState.messageToUser.denyAction;
            if (denyAction) {
                dispatch(denyAction);
            } else {
                console.warn('Deny action is not set');
            }

        }
    }
}

const SubscriptionManagementMessageToUser = connect(
    mapStateToProps,
    mapDispatchToProps
    )(Baloon);
SubscriptionManagementMessageToUser.displayName = 'SubscriptionManagementMessageToUser';

export default SubscriptionManagementMessageToUser;
