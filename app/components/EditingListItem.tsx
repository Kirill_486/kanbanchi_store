import * as React from 'react';
import { IEditingListItemProps } from '../types/components';
import SVG from './SVG';

export const EditingListItem: React.SFC<IEditingListItemProps> = (props) => {
    return (
        <div className="editing-item__container">
            <span
                    className="icon icon--g subscription-item__user-avatar">
                    <SVG xlink="svgicon--user"/>
            </span>
            <input
                value={props.value}
                onChange={props.onValueChanged}
                type="text"
                className="editing-item__input"/>
        </div>
    );
};
