import * as React from 'react';
import { IApplicationState } from '../types/types';
import { connect } from 'react-redux';
import ListItem from './SubscriptionManagementLicenseListItem';
import EditingListItem from './SubscriptionManagementEditingListItem'
import { ISubscriptionManagementListItemStateProps, IListItemOwnProps, ISubscriptionManagementListItemProps } from '../types/components';

export const SubscriptionManagementListItem: React.SFC<ISubscriptionManagementListItemProps> = (props) => {
    return (
        <div
            className="admin-list-item__container"
            >
            {props.isEditing ?
                <EditingListItem id={props.id}/> :
                <ListItem id={props.id}/>
            }
        </div>
    );
}

const mapStateToProps = (
    state: IApplicationState,
    ownProps: IListItemOwnProps
): ISubscriptionManagementListItemStateProps => {
    return {
        isEditing: state.subscriptionManagement.editingItemId === ownProps.id
    }
}

const connectedSubscriptionManagementGridListItem = connect(mapStateToProps)(SubscriptionManagementListItem);
connectedSubscriptionManagementGridListItem.displayName = 'SubscriptionManagementListItem';

export default connectedSubscriptionManagementGridListItem;
