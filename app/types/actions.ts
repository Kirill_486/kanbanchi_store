import { Action } from 'redux';
import { ISubscription, ISubscriptionItem } from './types';

export interface ISubscriptionAction extends Action {
    payload: {
        subscription: ISubscription;
        items: ISubscriptionItem[];
    }
};

export interface IItemsAction extends Action {
    payload: ISubscriptionItem[];
};

export interface IExceptionAction extends Action {
    payload: any;
};

export interface ISubscriptionManagementTextAction extends Action {
    payload: string;
};

export interface ISubscriptionManagementIdAction extends Action {
    payload: number;
};

interface IUserMessagePayload {
    message?: string;
    confirmAction: Action;
    denyAction: Action;
};

export interface IUserMessageAction extends Action {
    payload: IUserMessagePayload
};

interface IActionWithConfirmationPayload extends IUserMessagePayload {
    id: number;
};

export interface IActionWithConfirmation extends IUserMessageAction {
    payload: IActionWithConfirmationPayload;
};
