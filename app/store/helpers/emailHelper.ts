const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const breakingSymbols = /[^a-zA-Z0-9@.]/;

export const recognizeEmails = (input: string): string[] => {
    if (typeof input !== 'string') throw `Unexpected argument type ${typeof input}`;

    const stringParts = input.split(breakingSymbols);
    const emails = [];

    for (let part of stringParts) {
        if (emailRegexp.test(part))  {
            emails.push(part);
        }
    }

    return emails;
};
