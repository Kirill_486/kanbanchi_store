import { Action, ActionCreator } from 'redux';
import { ISubscription, ISubscriptionItem, IApplicationState } from '../../types/types';
import {subscriptionActionTypes} from '../constants';
import { makeSubscriptionItem, patchItems, getItemsBySubscriptionId } from '../helpers/subscriptionHelper';
import store from '../configureStore';
import { availableItemsSelector, usedItemsSelector } from '../selectors/itemsSelector';
import { changeInput } from './subscriptionManagementActions';
import { ISubscriptionAction, IItemsAction, IExceptionAction } from '../../types/actions';

export const beginSelect: ActionCreator<Action> =
() => ({
    type: subscriptionActionTypes.BEGIN_SELECT
});

export const syncWithMoodel: ActionCreator<Action> =
() => ({
    type: subscriptionActionTypes.SYNC_MODEL
});

export const selectSubscription: ActionCreator<ISubscriptionAction> =
(subscription: ISubscription, items: ISubscriptionItem[]) => {
    return {
        type: subscriptionActionTypes.SELECT_SUBSCRIPTION,
        payload: {
            subscription,
            items
        }
    };
};

export const asyncSelectSubscription: ActionCreator<any> =
(id?: number) => {
    const action = (dispatch: any) => {
        dispatch(beginSelect());
        dispatch(syncWithMoodel());
        const appState = store.getState() as IApplicationState;
        const subscriptions = appState.subscription.subscriptions;
        if (!id) {
            //provide initial value
            id = subscriptions[0].id;
        }
        const subscription = subscriptions.find(item => item.id === id);
        getItemsBySubscriptionId(id)
            .then((data) => {
                const items = data;
                dispatch(selectSubscription(
                    subscription,
                    items
                    )
                )
            })
            .catch((reason) => {
                console.error(reason);
                dispatch(exception());
            })
    }
    return action;
}

export const beginMoodify: ActionCreator<Action> =
() => ({
    type: subscriptionActionTypes.BEGIN_MODIFY
});

export const updateMembers: ActionCreator<IItemsAction> =
(items: ISubscriptionItem[]) => ({
    type: subscriptionActionTypes.UPDATE_MEMBERS,
    payload: items
});

export const exception: ActionCreator<IExceptionAction> =
(reason?: any) => ({
    type: subscriptionActionTypes.EXCEPTION,
    payload:reason
});

export const asyncPatchItemsAction: ActionCreator<any> =
(items: ISubscriptionItem[]) => {
    const action = (dispatch: any) => {
        dispatch(beginMoodify());
        const appState = store.getState() as IApplicationState;
        patchItems(appState.subscription.subscription.id, items)
        .then((data)=> {
            dispatch(updateMembers(data))
        })
        .catch((reason) => {
            dispatch(exception(reason))
        });
        dispatch(changeInput());
    }
    return action;
};

export const asyncAddItemsAction: ActionCreator<any> =
() => {
    const action = (dispatch: any) => {

        const appState = store.getState() as IApplicationState;
        const availableSeats = availableItemsSelector(appState);
        const usedSeatc = usedItemsSelector(appState);

        const recognizedEmails = appState.subscriptionManagement.recognizedItems;

        const currentEmails = usedSeatc.map((item) => item.email);
        const uniqueEmails = recognizedEmails.filter((item) => !currentEmails.includes(item));

        uniqueEmails.forEach((item, index) => {
            availableSeats[index].email = item;
        });

        const items: ISubscriptionItem[] = [
            ...usedSeatc,
            ...availableSeats
        ];
        dispatch(asyncPatchItemsAction(items));
    }
    return action;
};

export const asyncEditItemAction: ActionCreator<any> =
(
    subscriptionItem: ISubscriptionItem
) => {
    const action = (dispatch: any) => {
        const currentItems =  (store.getState() as IApplicationState).subscription.items;
        const items = currentItems.map((item) => {
            if (item.id ===  subscriptionItem.id) {
                return {
                    ...item,
                    ...subscriptionItem
                };
            } else {
                return item;
            }
        });
        dispatch(asyncPatchItemsAction(items));
    };
    return action;
}

export const asyncDeleteItemAction: ActionCreator<any> =
(
    id: number
) => {
    const action = (dispatch: any) => {
        const currentItems =  (store.getState() as IApplicationState).subscription.items;
        const items = currentItems.filter((item) => item.id !== id);
        dispatch(asyncPatchItemsAction(items));
    }
    return action;
}
