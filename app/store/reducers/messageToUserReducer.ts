import { Action } from 'redux';
import store from '../configureStore';
import { IUserMessageState } from '../../types/types';
import { messageToUserActionTypes } from '../constants';
import { IUserMessageAction } from '../../types/actions';

const initialState: IUserMessageState = {
    isShown: false,
    message: 'userMessageTest',
    confirmAction: null,
    denyAction: null
}

const messageToUserReducer = (
    state: IUserMessageState = initialState,
    action: Action): IUserMessageState => {
        switch (action.type) {
            case messageToUserActionTypes.SHOW_MESSAGE:
                const {message, confirmAction, denyAction} = (action as IUserMessageAction).payload;
                return {
                    ...state,
                    isShown: true,
                    message,
                    confirmAction,
                    denyAction

                };
            case messageToUserActionTypes.HIDE_MESSAGE:
                return initialState;
            case messageToUserActionTypes.CONFIRM: {
                const confirmAction = state.confirmAction;
                store.dispatch(confirmAction);
                return initialState;
            }
            default:
                return initialState;
        }
};

export default messageToUserReducer;

///todo finish message to user actions and reduser
