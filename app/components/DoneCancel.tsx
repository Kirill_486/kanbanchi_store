import * as React from 'react';
import {IMessageToUserProps} from '../types/components'

export const DoneCansel: React.SFC<IMessageToUserProps> =
(props) => {
    return props.isShown && (
        <div className="done-cancel__container">
            <button
                className="
                    done-cancel__button
                    done-cancel__button-done"
                onClick={props.onConfirm}>
                Done
            </button>
            <button
                className="
                    done-cancel__button
                    done-cancel__button-cancel"
                onClick={props.onDeny}>
                Cansel
            </button>
        </div>
    )
};
