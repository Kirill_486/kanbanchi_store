import * as React from 'react';

export const NothingFoundScreen: React.SFC<{}> = () => {
    return (
        <div className="nothing-found__container">
            <span className="nothing-found__logo">
                <strong> No seats found </strong>
            </span>
            <p className="nothing-found__text">
                We couldn't find any seats,
                try another search.
            </p>
        </div>
    )
};
