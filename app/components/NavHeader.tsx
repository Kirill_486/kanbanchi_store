import * as React from 'react';
import {INavProps, INavState} from '../types/components'
import {SelectBase as Select} from 'react-select';
import SVG from '../components/SVG';

const initialState: INavState = {
    isDropDownOpen: false
};

export class NavHeader extends React.Component<INavProps, INavState> {

    constructor() {
        super();
        this.state = initialState;
    }

    setOption = (option: any) => {
        this.props.selectSubscription(option.value);
        this.setState({
            ...this.state,
            isDropDownOpen: false
        });
    }

    openDropDown = () => {
        this.setState({
            ...this.state,
            isDropDownOpen: true
        });
    }

    closeDropDown = () => {
        this.setState({
            ...this.state,
            isDropDownOpen: false
        });
    }

    render() {
        const currentOption = this.props.availableProductsOptions.find(item => item.value === this.props.currentOption);
        return (
            <div className="nav-header__container">

            <span className="icon nav-header__close-dialog"
                    onClick={this.props.goBack}>
                <SVG xlink="svgicon--arrow-back"/>
            </span>
            <Select
                className="nav-header__drop-down"
                options={this.props.availableProductsOptions}
                isDisabled={this.props.availableProductsOptions.length < 2}
                value={currentOption}
                menuIsOpen={this.state.isDropDownOpen}
                onChange={this.setOption}
                onMenuOpen={this.openDropDown}
                onMenuClose={this.closeDropDown}
                onInputChange={() => true}
            />
            <div className="nav-header__user-icon">
                <img className="nav-header__userpic" src={''} alt="userPic"/>
            </div>
        </div>
        );
    }
};

//NavHeader.displayName = 'NavHeader';
