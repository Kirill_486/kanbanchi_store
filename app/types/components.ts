import { ISubscriptionItem } from './types';

export interface IGridStateProps {
    items?: ISubscriptionItem[];
    component?: any;
}

export interface IGridProps extends IGridStateProps {

}

export interface ISubscriptionManagementListItemOwnProps extends IListItemOwnProps {
    listItemComponent: any;
    editingListItemComponent: any;
}

export interface ISubscriptionManagementListItemStateProps {
    isEditing?: boolean;
}

export interface ISubscriptionManagementListItemProps extends
ISubscriptionManagementListItemOwnProps,
ISubscriptionManagementListItemStateProps {
}

export interface IListItemOwnProps {
    id: number;
}

export interface IListItemStateProps {
    email?: string;
    areButtonsShowing?: boolean;
}

export interface IListItemDispatchProps {
    startEdit?: () => void;
    startDelete?: () => void;
}

export interface IListItemProps extends
IListItemOwnProps,
IListItemStateProps,
IListItemDispatchProps {

}

export interface ISummaryStateProps {
    seatsAvailable?: number;
    seatsUsed?: number;
    expireDate?: string;
    billingType?: string;
    billingWording?: string;
    placeholderWording: string;
}

export interface ISummaryDispatchProps {
    manageSubscription?: () => void;
}

export interface ISummaryProps extends ISummaryStateProps, ISummaryDispatchProps {

}

export interface IMessageToUserStateProps {
    isShown?: boolean;
    message?: string;
    confirmAction?: any;
    denyAction?: any;
}

export interface IMessageToUserDispatchProps {
    onConfirm?: () => void;
    onDeny?: () => void;
}

export interface IMessageToUserProps extends
IMessageToUserStateProps,
IMessageToUserDispatchProps {

}

export interface INavMapStateToProps {
    availableProductsOptions?: ISelectOption[];
    currentOption: number;
}

export interface INavMapDispatchToProps {
    goBack: () => void;
    selectSubscription: (id: number) => void;
}

export interface INavProps extends INavMapStateToProps, INavMapDispatchToProps {

}

export interface ISelectOption {
    value: number;
    label: string;
}

export interface IAddItemsStateProps {
    isShown?: boolean;
    isInputOpen?: boolean;
    inputValue?: string;
    recognizedItems?: string[];
    seatsAvailable?: number;
    placeholderWording?: string;
}

export interface IAddItemsDispatchProps {
    openInput?: () => void;
    closeInput?: () => void;
    inputChamge?: (e: any) => void;
    addMembers?: () => void;
}

export interface IAddItemsProps extends IAddItemsStateProps, IAddItemsDispatchProps {

}

export interface ISearchStateProps {
    isSearchOpen: boolean;
    searchString: string;
}

export  interface ISearchDispatchProps {
    openSearch: () => void;
    closeSearch: () => void;
    changeSearch: (e: any) => void;
    clearSearch: () => void;
}

export interface ISearchProps extends ISearchStateProps, ISearchDispatchProps {

}

export interface IEditingListItemOwnProps {
    id: number;
}

export interface IEditingListItemStateProps {
    value?: string;
}

export interface IEditingListItemDispatchProps {
    onValueChanged?: (e: any) => void,
    onSaveItem?: () => void;
    onCancelChanges?: () => void;
}

export interface IEditingListItemProps extends
IEditingListItemOwnProps,
IEditingListItemStateProps,
IEditingListItemDispatchProps { }

export interface INavState {
    isDropDownOpen: boolean;
}
