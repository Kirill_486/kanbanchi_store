export const root = window as any;

export const subscriptionActionTypes = Object.freeze({
    BEGIN_SELECT: 'BEGIN_SELECT',
    SYNC_MODEL: 'SYNC_MODEL',
    SELECT_SUBSCRIPTION: 'SELECT_SUBSCRIPTION',
    BEGIN_MODIFY: 'BEGIN_MODIFY',
    UPDATE_MEMBERS: 'UPDATE_MEMBERS',
    EXCEPTION: 'EXCEPTION'
});

export const subscriptionManagementActionTypes = Object.freeze({
    OPEN_INPUT: 'OPEN_INPUT',
    CLOSE_INPUT: 'CLOSE_INPUT',
    CHANGE_INPUT: 'CHANGE_INPUT',
    CLEAR_INPUT: 'CLEAR_INPUT',

    OPEN_SEARCH: 'OPEN_SEARCH',
    CLOSE_SEARCH: 'CLOSE_SEARCH',
    CHANGE_SEARCH: 'CHANGE_SEARCH',
    CLEAR_SEARCH: 'CLEAR_SEARCH',

    OPEN_EDITING: 'OPEN_EDITING',
    CLOSE_EDITING: 'CLOSE_EDITING',
    CHANGE_EDITING: 'CHANGE_EDITING',
    APPLY_EDITING: 'APPLY_EDITING',

    SHOW_DONE_CANCEL: 'SHOW_DONE_CANCEL',
    HIDE_DONE_CANCEL: 'HIDE_DONE_CANCEL',
    CONFIRM_ACTION: 'CONFIRM_ACTION'
});

export const messageToUserActionTypes = Object.freeze({
    SHOW_MESSAGE: 'SHOW_MESSAGE',
    HIDE_MESSAGE: 'HIDE_MESSAGE',
    CONFIRM: 'CONFIRM'
});

export const chargeBeeProds = [13, 14, 15, 16];
export const starterPlans = [13, 14];
export const monthlyPlans = [13, 15];

export const billingWordingOptions = Object.freeze({
    newxtBilling: 'Next billing date ',
    expirationDate: 'Expiration date ',
    expired: 'Exriped '
});
