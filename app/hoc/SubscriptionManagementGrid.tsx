import { IApplicationState } from '../types/types';
import { selectItems } from '../store/selectors/itemsSelector';
import { connect } from 'react-redux';
import { IGridStateProps } from '../types/components';

import {Grid} from '../components/Grid';
import SubscriptionManagementGridListItem from './SubscriptionManagementListItem';

const mapStateToProps = (state: IApplicationState): IGridStateProps => {
    return {
        items: selectItems(state),
        component: SubscriptionManagementGridListItem
    }
}

const SubscriptionItemsGrid = connect(mapStateToProps, undefined)(Grid);
SubscriptionItemsGrid.displayName = 'SubscriptionItemsGrid';

export default SubscriptionItemsGrid;
