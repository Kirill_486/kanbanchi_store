import {connect} from 'react-redux';
import { IApplicationState } from '../types/types';
import { openSearch, closeSearch, changeSearch, clearSearch } from '../store/actions/subscriptionManagementActions';
import { ISearchStateProps, ISearchDispatchProps } from '../types/components';
import {Search} from '../components/Search';

const mapStateToProps = (state: IApplicationState): ISearchStateProps => {
    return {
        isSearchOpen: state.subscriptionManagement.isSearchOpen,
        searchString: state.subscriptionManagement.searchingString
    };
};

const mapDispatchToProps = (dispatch: any): ISearchDispatchProps => {
    return {
        openSearch: () => dispatch(openSearch()),
        closeSearch: () => dispatch(closeSearch()),
        changeSearch: (e: any) => {
            const value = e.target.value;
            dispatch(changeSearch(value))
        },
        clearSearch: () => dispatch(clearSearch())
    }
}

const SubscriptionManagementSearch = connect(mapStateToProps, mapDispatchToProps)(Search);
SubscriptionManagementSearch.displayName = 'SubscriptionManagementSearch';

export default SubscriptionManagementSearch;
