import { IApplicationState } from '../types/types';
import { INavMapDispatchToProps, INavMapStateToProps } from '../types/components';
import { connect } from 'react-redux';
import { getSubscriptionIdByProductId, getProductSelectOptions, selectCurrentOption } from '../store/selectors/productsSelector';
import { asyncSelectSubscription } from '../store/actions/subscriptionActions';
import { NavHeader } from '../components/NavHeader';
import { resetState } from '../store/actions/subscriptionManagementActions';

const mapStateToProps =
(
    state: IApplicationState
): INavMapStateToProps => {
    const availableProductsOptions = getProductSelectOptions();
    const currentOption = selectCurrentOption();

    return {
        availableProductsOptions,
        currentOption
    }
}

const mapDispatchToProps =
(dispatch: any): INavMapDispatchToProps => {
    return {
        goBack: () => console.log('go Back'),
        selectSubscription: (productId: number) => {
            const subscriptionId = getSubscriptionIdByProductId(productId);
            dispatch(resetState());
            dispatch(asyncSelectSubscription(subscriptionId));
        }
    }
};

const SubscriptionManagementNavHeader = connect(mapStateToProps, mapDispatchToProps)(NavHeader);
SubscriptionManagementNavHeader.displayName = 'SubscriptionManagementNavHeader';

export default SubscriptionManagementNavHeader;
