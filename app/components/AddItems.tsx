import * as React from 'react';
import SVG from './SVG';
import { IAddItemsProps } from '../types/components';

export const AddItems: React.SFC<IAddItemsProps> = (props) => {
    const linesNumber = props.inputValue.split('\n').length;
    const recognizedItemsLength = props.recognizedItems.length;
    const areThereEnoughtSeats = props.seatsAvailable >= recognizedItemsLength;
    const isAddMembersEnabled = !!recognizedItemsLength && areThereEnoughtSeats;
    return props.isShown && (
        <div className="add-items__main">
            {!props.isInputOpen ? (
                <span
                    className="add-items__add-button"
                    onClick={props.openInput}
                >
                    <span className="icon icon--g">
                        <SVG xlink="svgicon--plus" />
                    </span>
                </span>
                ) : false}
            {props.isInputOpen ? (
                <div className="add-items__main">
                    <textarea
                        className="add-items__textarea"
                        name="items"
                        rows={linesNumber}
                        value={props.inputValue}
                        placeholder={props.placeholderWording}
                        onChange={props.inputChamge}
                    >
                    </textarea>
                    <button
                        className="add-items__add-members"
                        type="button"
                        disabled={!isAddMembersEnabled}
                        onClick={() => {
                            props.addMembers();
                            props.closeInput();
                        }}
                    >
                        {`Add${props.recognizedItems.length > 0 ? ` ${props.recognizedItems.length} ` : ' '}members`}
                    </button>
                </div>
                ) : false}
        </div>

    );
};
