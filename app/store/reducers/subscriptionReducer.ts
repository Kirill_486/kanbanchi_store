import { Action } from 'redux';
import { ICurrentSubscription} from '../../types/types';
import {subscriptionActionTypes} from '../constants';
import {getActiveSubscriptions} from '../helpers/subscriptionHelper';
import {root} from '../constants'
import { ISubscriptionAction, IItemsAction } from '../../types/actions';

const initialState: ICurrentSubscription = {
    subscriptions: [],
    subscription: undefined,
    items: []
}

const subscriptionReducer =
(
    state: ICurrentSubscription = initialState,
    action: Action | ISubscriptionAction | IItemsAction
) => {
    let items = [];
    switch (action.type) {
        case subscriptionActionTypes.BEGIN_SELECT:
            return  {
                ...state,
                subscription: undefined,
                items: []
            };
        case subscriptionActionTypes.SYNC_MODEL:
            const activeSubscriptions = getActiveSubscriptions(root.App);
            return  {
                ...state,
                subscriptions: activeSubscriptions
            };
        case subscriptionActionTypes.SELECT_SUBSCRIPTION:
            const subscriptionAction = action as ISubscriptionAction;
            const subscription = subscriptionAction.payload.subscription;
            items = subscriptionAction.payload.items;
            return {
                ...state,
                subscription,
                items
            }
        case subscriptionActionTypes.BEGIN_MODIFY:
            return state;
        case subscriptionActionTypes.UPDATE_MEMBERS:
            const itemsAction = action as IItemsAction;
            items = itemsAction.payload;
            return {
                ...state,
                items
            }
        case subscriptionActionTypes.EXCEPTION:
            return initialState;
        default: return state;
    }
}

export default subscriptionReducer;
