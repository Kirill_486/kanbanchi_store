import { ActionCreator } from 'react-redux';
import { Action } from 'redux';
import { messageToUserActionTypes } from '../constants';
import { IUserMessageAction } from '../../types/actions';

export const showUserMessage: ActionCreator<IUserMessageAction> = (
    message: string,
    confirmAction: Action,
    denyAction: Action
) => {
    return {
        type: messageToUserActionTypes.SHOW_MESSAGE,
        payload: {
            message,
            confirmAction,
            denyAction
        }
    }
};

export const hideUserMessage: ActionCreator<Action> = () => {
    return {
        type: messageToUserActionTypes.HIDE_MESSAGE
    }
};

export const confirm: ActionCreator<Action> =() => {
    return {
        type: messageToUserActionTypes.CONFIRM
    }
}
