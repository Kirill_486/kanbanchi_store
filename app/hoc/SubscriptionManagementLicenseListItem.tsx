import { IApplicationState, ISubscriptionItem } from '../types/types';
import { connect } from 'react-redux';
import { showUserMessage, hideUserMessage } from '../store/actions/messageToUserActions';
import { asyncDeleteItemAction, asyncEditItemAction } from '../store/actions/subscriptionActions';

import {ListItem} from '../components/ListItem';

import store from '../store/configureStore';
import { openEditing, resetState, closeEditing, showDoneCansel } from '../store/actions/subscriptionManagementActions';
import { IListItemOwnProps, IListItemStateProps, IListItemDispatchProps } from '../types/components';

const mapStateToProps = (
    state: IApplicationState,
    props: IListItemOwnProps
): IListItemStateProps => {
    const email = state
        .subscription
        .items
        .find(
            (item) => item.id === props.id
        ).email;
    const areButtonsShowing =
        !state.subscriptionManagement.inputOpen &&
        !state.subscriptionManagement.isItemEditing;
    return {
        email,
        areButtonsShowing
    };
};

const mapDispatchToProps = (
    dispatch: any,
    props: IListItemOwnProps
    ): IListItemDispatchProps => {
    const id = props.id;
    return {
        startEdit: () => {
            const appState = store.getState() as IApplicationState;
            const currentItem = appState.subscription.items.find((item) => item.id === id)
            dispatch(showDoneCansel(
                id,
                asyncEditItemAction(currentItem),
                closeEditing()
            ));
        },
        startDelete: () => {
            const appState = store.getState() as IApplicationState;
            const isUndoShowing = !!(appState.messageToUser.isShown && appState.messageToUser.confirmAction);
            if (isUndoShowing) {
                const currentAction = appState.subscriptionManagement.confirmAction;
                dispatch(hideUserMessage());
                dispatch(currentAction);
            }
            const item = appState.subscription.items.find(item => item.id === props.id);
            dispatch(showUserMessage(
                `Are you sure to delete ${item.email}`,
                asyncDeleteItemAction(props.id),
                hideUserMessage()
            ))
        }
    };
};

const SubscriptionManagementListItem = connect(mapStateToProps, mapDispatchToProps)(ListItem);
SubscriptionManagementListItem.displayName = 'SubscriptionManagementListItem';

export default SubscriptionManagementListItem;
