import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import store from './store/configureStore';
import SubscriptionManagement from './hoc/SubscriptionManagement';

import './styles/main.scss';

const appRoot = document.getElementById('app');

ReactDOM.render(
    <Provider store={store}>
        <SubscriptionManagement />
    </Provider>,
    appRoot
);
