import {connect} from 'react-redux';
import { IApplicationState } from '../types/types';
import { ISummaryStateProps, ISummaryDispatchProps } from '../types/components';
import { getBillingTypeById, getBillingWording, getDateHumanReadable } from '../store/selectors/productsSelector';
import { availableItemsSelector, usedItemsSelector } from '../store/selectors/itemsSelector';
import { Summary } from '../components/Summary';

const mapStateToProps = (state: IApplicationState): ISummaryStateProps => {
    const isSubscriptionSet = !!state.subscription.subscription;
    let billingType, billingWording, expireDate, expireDateHumanReadable;
    const placeholderWording = 'Type emails one by one or insert a list of users'
    if (isSubscriptionSet) {
        billingType = getBillingTypeById(state.subscription.subscription.productId);
        billingWording = getBillingWording();
        expireDate = state.subscription.subscription.paymentExpDate;
        expireDateHumanReadable = getDateHumanReadable(expireDate);
    }
    else {
        expireDateHumanReadable = '';
        billingWording = '';
        expireDate = '';
    }
    return {
        expireDate: expireDateHumanReadable,
        seatsAvailable: availableItemsSelector(state).length,
        seatsUsed: usedItemsSelector(state).length,
        billingType,
        billingWording,
        placeholderWording
    }
}

const mapDispatchToProps = (dispatch: any): ISummaryDispatchProps => {
    return {
        manageSubscription: () => console.log('manageSubscription')
    }
}

const connectedSummary = connect(mapStateToProps, mapDispatchToProps)(Summary);
connectedSummary.displayName = 'SubscriptionManagementSummary';
export default connectedSummary;
