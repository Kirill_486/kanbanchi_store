import { IApplicationState } from '../types/types';
import store from '../store/configureStore';
import { DoneCansel } from '../components/DoneCancel';
import {connect} from 'react-redux'
import { IMessageToUserStateProps, IMessageToUserDispatchProps } from '../types/components';

const mapStateToProps = (
    state: IApplicationState
    ): IMessageToUserStateProps => {
    return {
        //isShown: state.messageToUser.isShown,
        isShown: state.subscriptionManagement.isItemEditing,
        message: state.messageToUser.message
    }
}

const mapDispatchToProps = (
    dispatch: any
    ): IMessageToUserDispatchProps => {
    return {
        onConfirm: () => {
            const appState = (store.getState() as IApplicationState);
            const confirmAction = appState.subscriptionManagement.confirmAction;
            if (confirmAction) {
                dispatch(confirmAction);
            }else {
                console.warn('Confirmation action is not set');
            }
        } ,
        onDeny: () => {
            const appState = (store.getState() as IApplicationState);
            const denyAction = appState.subscriptionManagement.denyAction;
            if (denyAction) {
                dispatch(denyAction);
            } else {
                console.warn('Deny action is not set');
            }
        }
    }
}

const SubscriptionManagementMessageToUser = connect(
    mapStateToProps,
    mapDispatchToProps
    )(DoneCansel);
SubscriptionManagementMessageToUser.displayName = 'SubscriptionManagementDoneCancel';

export default SubscriptionManagementMessageToUser;
