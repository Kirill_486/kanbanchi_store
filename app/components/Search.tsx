import * as React from 'react';
import SVG from './SVG';
import { ISearchProps } from '../types/components';

export const Search: React.SFC<ISearchProps> = (props) => {
    return (
        <div
            className="search__container"
                >
                <span className="icon icon-g search__icon">
                    <SVG xlink="svgicon--search"/>
                </span>
            <input
                className="search__input"
                type="text"
                name="searchExpression"
                placeholder="Search"
                onChange={props.changeSearch}
                value={props.searchString}
            />
            { props.searchString &&
            (
            <button
                className="search__clear"
                onClick={props.clearSearch}>
            X
            </button>
            )}
        </div>
    );
};

Search.displayName = 'Search';
