import * as React from 'react';
import {IMessageToUserProps} from '../types/components'

export const Baloon: React.SFC<IMessageToUserProps> =
(props) => {
    return props.isShown && (
        <div className="message__container">
            <span className="message__message">
                {props.message}
            </span>
            <div className="message__controls">
                <button
                    className="message__control"
                    onClick={props.onConfirm}>
                Y
                </button>
                <button
                    className="message__control"
                    onClick={props.onDeny}>
                N
                </button>
            </div>
        </div>
    )
};
