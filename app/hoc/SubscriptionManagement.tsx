import * as React from 'react';
import NavHeader from './SubscriptionManagementNavHeader';
import Summary from './SubscriptionManagementSummary'
import Search from './SubscriptionManagementSearch';
import SubscriptionItemsGrid from './SubscriptionManagementGrid';
import AddItems from './SubscriptionManagementAddItems';
import SubscriptionManagementMessageToUser from './SubscriptionManagementUndoBaloon';
import { asyncSelectSubscription } from '../store/actions/subscriptionActions';
import SubscriptionManagementDoneCancel from './SubscriptionManagementDoneCancel';
import { connect } from 'react-redux';

class SubscriptionManagement extends React.Component<IPaymentDialogProps, {}> {
    componentDidMount() {
        this.props.selectSubscription();
    }
    render() {
        return (
            <div className="admin-panel__main">
                <NavHeader/>
                <div className="admin-panel__container">
                    <Summary />
                    <Search />
                    <SubscriptionItemsGrid />
                    <SubscriptionManagementDoneCancel />
                    <AddItems />
                </div>
                <SubscriptionManagementMessageToUser />
            </div>
        )
    }
};

export interface IPaymentDialogDispatchProps {
    selectSubscription?: () => void;
}

export  interface IPaymentDialogProps extends IPaymentDialogDispatchProps {

}

const mapDispatchToProps = (dispatch: any): IPaymentDialogDispatchProps => {
    return {
        selectSubscription: () => dispatch(asyncSelectSubscription())
    }
}

const connectedPaymentDialog = connect(undefined, mapDispatchToProps)(SubscriptionManagement);
connectedPaymentDialog.displayName = 'SubscriptionManagement';

export default connectedPaymentDialog;
