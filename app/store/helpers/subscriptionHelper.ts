import {IProduct, ISubscription, ISubscriptionItem, IApplicationState} from '../../types/types';
import {root} from '../constants';
import {items60, items61, activeSubscriptions} from '../defaults';
import store from '../configureStore';

export const getProductName = (productId: number) => {
    const products = root.App.model.get('authUser').get('products') as IProduct[];
    const prod = products.find((product: IProduct) => product.id === productId);
    return prod ? prod.name : '';
};

export const getItemsUrl = (subscriptionId: number): string => {
    return `/rest/subscriptions/items/${subscriptionId}`;
};

export const getItemsBySubscriptionId = (_subscriptionId: number): Promise<ISubscriptionItem[]> => {
    const subscriptionItems = (_subscriptionId % 10) === 1 ? items61: items60;
    return Promise.resolve(subscriptionItems);
};

export const patchItems = (_subscriptionId: number, items: ISubscriptionItem[]) => {
    return Promise.resolve([
        ...items
    ]);
}
// #todo remove dummy
// export const getUserId = (model: any): number => model.get('authUser').id;
export const getUserId = (_model: any): number => 42;

// #todo remove dummy
// export const getActiveSubscriptions = (model: any): ISubscription[] => {
//     const result = model.get('authUser').get('activeSubscription');
//     return result;
// };

export const getActiveSubscriptions = (_model: any): ISubscription[] => activeSubscriptions;

// #todo remove dummy
// export const getUserFullName = root.App.model.get('authUser').get('fullName');
export const getUserFullName = 'John Doe';

// #todo remove dummy
// export const userpicUrl = root.App.model.get('authUser').get('photoUrl');
export const userpicUrl = 'photoUrl';

export const isItemInSearchResults =
(
    item: ISubscriptionItem,
    query: string
): boolean => {
    return item.email.includes(query);
};

let id = 100;

// todo get id from server
export const makeSubscriptionItem = (
    email: string,
    subscriptionId: number
): ISubscriptionItem => {
    return {
        email,
        id: ++id,
        subscriptionId
    }
};
