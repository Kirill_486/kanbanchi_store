import * as React from 'react';
import { IGridProps } from '../types/components';
import { NothingFoundScreen } from './NothingFoundScreen';

export const Grid = (props: IGridProps) => {
    const ListItem = props.component;
    const areAnyItemsInGrid = props.items && props.items.length > 0;
    return (
        <div className="grid__container">
            {areAnyItemsInGrid ?
                props.items && props.items.map((item) => {
                    return (
                        <ListItem
                            key={item.id}
                            id={item.id}
                        />
                    );
                })
                :
                (
                    <NothingFoundScreen />
                )
            }
        </div>
    );
};
