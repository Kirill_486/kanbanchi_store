import {ISubscription,ISubscriptionItem, IProduct} from '../types/types';

export const items60: ISubscriptionItem[] = [
    {
        'id':4573968371548160,
        'subscriptionId':6333186975989760
    },
    {
        'id':4925812092436480,
        'email':'onlymectb24@gmail.com',
        'subscriptionId':6333186975989760
    },
    {
        'id':5092937859858432,
        'subscriptionId':6333186975989760
    },
    {
        'id':5699868278390784,
        'subscriptionId':6333186975989760
    },
    {
        'id':6218837766701056,
        'subscriptionId':6333186975989760
    },
    {
        'id':6614661952700416,
        'subscriptionId':6333186975989760
    }
];

export const items61: ISubscriptionItem[] = [
    {
        'id':4573968371548160,
        'subscriptionId':6333186975989761
    },
    {
        'id':4925812092436480,
        'email':'you@cool.com',
        'subscriptionId':6333186975989761
    },
    {
        'id':5092937859858432,
        'email': 'hands@fromShoulders.biz',
        'subscriptionId':6333186975989761
    },
    {
        'id':5699868278390784,
        'subscriptionId':6333186975989761
    },
    {
        'id':6218837766701056,
        'subscriptionId':6333186975989761
    },
    {
        'id':6614661952700416,
        'subscriptionId':6333186975989761
    }
];

export const subscription1: ISubscription = {
    'id':6333186975989760,
    'status':'Active',
    'created':1542010694,
    'productId':13,
    'paymentExpDate':1573627719
};

export const subscription2: ISubscription = {
    'id':6333186975989761,
    'status':'Active',
    'created':1542010694,
    'productId':15,
    'paymentExpDate':1583628000
};

export const activeSubscriptions = [subscription1, subscription2];

export const products: IProduct[] =
[
    {
      'id': 1,
      'name': 'Gantt chart- month'
    },
    {
      'id': 2,
      'name': 'Gantt chart- quarter'
    },
    {
      'id': 3,
      'name': 'Gantt chart- year'
    },
    {
      'id': 4,
      'name': 'Time Tracker- month'
    },
    {
      'id': 5,
      'name': 'Time Tracker- quarter'
    },
    {
      'id': 6,
      'name': 'Time Tracker- year'
    },
    {
      'id': 7,
      'name': 'Branding- month'
    },
    {
      'id': 8,
      'name': 'Branding- quarter'
    },
    {
      'id': 9,
      'name': 'Branding- year'
    },
    {
      'id': 10,
      'name': 'All Paid Features- month'
    },
    {
      'id': 11,
      'name': 'All Paid Features- quarter'
    },
    {
      'id': 12,
      'name': 'All Paid Features- year'
    },
    {
      'id': 13,
      'name': 'Starter  Features- month'
    },
    {
      'id': 14,
      'name': 'Starter  Features- year'
    },
    {
      'id': 15,
      'name': 'Professional   Features- month'
    },
    {
      'id': 16,
      'name': 'Professional   Features- year'
    }
];
