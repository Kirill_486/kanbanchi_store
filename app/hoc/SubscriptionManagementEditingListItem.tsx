import { IApplicationState } from '../types/types';
import {connect} from 'react-redux';
import {closeEditing, changeEditing} from '../store/actions/subscriptionManagementActions';
import { showUserMessage, hideUserMessage } from '../store/actions/messageToUserActions';
import {EditingListItem} from '../components/EditingListItem';

import store from '../store/configureStore';
import { asyncEditItemAction } from '../store/actions/subscriptionActions';
import { IListItemOwnProps, IEditingListItemStateProps, IEditingListItemDispatchProps } from '../types/components';

const mapStateToProps = (
    state: IApplicationState
): IEditingListItemStateProps => {
    return {
        value: state.subscriptionManagement.editingValue
    }
}

const mapDispatchToProps = (
    dispatch: any,
    props: IListItemOwnProps
): IEditingListItemDispatchProps => {
    return {
        onValueChanged: (e: any) => {
            const newValue = e.target.value;
            dispatch(changeEditing(newValue));
        },
        onSaveItem: () => {
            const appState = store.getState() as IApplicationState;
            const currentItem = appState.subscription.items.find((item) => item.id === props.id)
            const newItemEmail = appState.subscriptionManagement.editingValue;
            currentItem.email = newItemEmail;
            dispatch(showUserMessage(
                `Are you sure you wanna change email to ${newItemEmail}`,
                () => {
                    dispatch(closeEditing());
                    dispatch(asyncEditItemAction(currentItem));
                },
                () => {
                    dispatch(closeEditing());
                    dispatch(hideUserMessage());
                }
            ));
        },
        onCancelChanges: () => {
            dispatch(closeEditing());
        }
    }
}

const connectedEditingItem = connect(mapStateToProps, mapDispatchToProps)(EditingListItem);

export default connectedEditingItem;
