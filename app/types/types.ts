import { Action } from 'redux';

export interface ICurrentSubscription {
    subscriptions: ISubscription[];
    subscription?: ISubscription;
    items: ISubscriptionItem[];
}

export interface ISubscriptionManagementState {
    inputOpen: boolean;
    inputString: string;
    recognizedItems: string[];

    isSearchOpen: false;
    searchingString: string;

    isItemEditing: boolean;
    editingItemId: number;
    editingValue: string;
    confirmAction: any;
    denyAction: any;
}

export interface IUserMessageState {
    isShown: boolean;
    message: string;
    confirmAction: Action;
    denyAction: Action;
}

export interface IApplicationState {
    subscription: ICurrentSubscription;
    subscriptionManagement: ISubscriptionManagementState;
    messageToUser: IUserMessageState;
}

export interface ISubscription {
    id: number;
    productId: number;
    created: number;
    paymentExpDate: number;
    status: string;
}

export interface ISubscriptionItem {
    id: number;
    subscriptionId: number;
    email?: string;
    isOk?: boolean;
    message?: string;
}

export interface IProduct{
    id: number;
    name: string;
};
