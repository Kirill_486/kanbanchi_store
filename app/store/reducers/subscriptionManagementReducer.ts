import { ISubscriptionManagementState, IApplicationState, ISubscriptionItem } from '../../types/types';
import {subscriptionManagementActionTypes} from '../constants';
import { Action } from 'redux';
import { recognizeEmails } from '../helpers/emailHelper';
import store from '../configureStore';
import { ISubscriptionManagementTextAction, ISubscriptionManagementIdAction, IUserMessageAction, IActionWithConfirmation } from '../../types/actions';
import { EditingListItem } from '../../components/EditingListItem';

const initialState: ISubscriptionManagementState = {
    //add members
    inputOpen: false,
    inputString: '',
    recognizedItems: [],

    isItemEditing: false,
    editingItemId: 0,
    editingValue: '',

    isSearchOpen: false,
    searchingString: '',
    confirmAction: null,
    denyAction: null
}

const subscriptionManagementReducer =
(
    state: ISubscriptionManagementState = initialState,
    action: Action | ISubscriptionManagementTextAction | ISubscriptionManagementIdAction
) => {
    switch (action.type) {
        case subscriptionManagementActionTypes.OPEN_INPUT:
            return {
                ...state,
                inputOpen: true,
                isSearchOpen: false,
                isItemEditing: false
            }
        case subscriptionManagementActionTypes.CLOSE_INPUT:
            return {
                ...state,
                inputOpen: false
            }
        case subscriptionManagementActionTypes.CHANGE_INPUT: {
            const textAction = action as ISubscriptionManagementTextAction;
            const appState = store.getState() as IApplicationState;
            const emailsInList = appState.subscription.items.map(item => item.email);
            const payload = textAction.payload || '';
            const recognizedEmails = recognizeEmails(payload);
            const recognizedItems = recognizedEmails.filter((item: string) => !emailsInList.includes(item));
            return {
                ...state,
                inputString: payload,
                recognizedItems
            }
        }
        case subscriptionManagementActionTypes.CLEAR_INPUT:
            return {
                ...state,
                inputOpen: false,
                inputString: ''
            }

        case subscriptionManagementActionTypes.OPEN_SEARCH:
            return {
                ...state,
                isSearchOpen: true,
                inputOpen: false,
                isItemEditing: false
            }
        case subscriptionManagementActionTypes.CLOSE_SEARCH:
            return {
                ...state,
                isSearchOpen: false
            }
        case subscriptionManagementActionTypes.CHANGE_SEARCH: {
            const textAction = action as ISubscriptionManagementTextAction;
            return {
                ...state,
                searchingString: textAction.payload,

            }
        }
        case subscriptionManagementActionTypes.OPEN_EDITING: {
            const idAction = action as ISubscriptionManagementIdAction;
            const editingItemId = idAction.payload;
            const items: ISubscriptionItem[] = (store.getState() as IApplicationState).subscription.items;
            const currentItem = items.find((item => item.id === editingItemId));
            return {
                ...state,
                inputOpen: false,
                isSearchOpen: false,
                isItemEditing: true,
                editingItemId,
                editingValue: currentItem.email
            }
        }
        case subscriptionManagementActionTypes.CLOSE_EDITING: {
            return {
                ...state,
                isItemEditing: false,
                editingItemId: 0,
                editingValue: ''
            }
        }
        case subscriptionManagementActionTypes.CHANGE_EDITING: {
            const textAction = action as ISubscriptionManagementTextAction;
            return {
                ...state,
                editingValue: textAction.payload
            }
        }
        case subscriptionManagementActionTypes.SHOW_DONE_CANCEL: {
            const userMessageAction = action as IActionWithConfirmation;
            const {confirmAction, denyAction} = userMessageAction.payload;
            const appState = store.getState() as IApplicationState;
            const items = appState.subscription.items;
            const targetItemId = userMessageAction.payload.id;
            const editedItem = items.find((item) => item.id === targetItemId);

            return {
                ...state,
                editingItemId: userMessageAction.payload.id,
                editingValue: editedItem.email,
                isItemEditing: true,
                confirmAction,
                denyAction
            }
        }
        case subscriptionManagementActionTypes.HIDE_DONE_CANCEL: {
            return {
                ...state,
                isItemEditing: false,
                confirmAction: {},
                denyAction: {}
            }
        }
        case subscriptionManagementActionTypes.CONFIRM_ACTION: {
            const confirmAction = state.confirmAction;
            store.dispatch(confirmAction);
            return state;
        }
        case subscriptionManagementActionTypes.CLEAR_SEARCH: {
            return {
                ...state,
                searchingString: ''
            }
        }
        default:
            return state;
    }
};

export default subscriptionManagementReducer;
