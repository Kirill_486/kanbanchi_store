import * as React from 'react';
import { ISummaryProps } from '../types/components';

export const Summary: React.SFC<ISummaryProps> = (props) => {
    return (
        <div className="summary__container">
            <div className="summary__section">

                <span className="summary__value">
                    {props.seatsAvailable}
                </span>

                <span className="summary__comment">
                    Seats available
                </span>

                <span className="summary__value">
                    {props.seatsUsed}
                </span>

                <span className="summary__comment">
                    Seats used
                </span>
            </div>

            <div className="summary__section">
                <span className="summary__billing">
                    {props.billingWording}: <strong>{props.expireDate}</strong>
                </span>
                <button className="summary__manage">
                    Manage subscriptions
                </button>
            </div>
        </div>
    );
};
