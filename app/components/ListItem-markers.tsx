import * as React from 'react';
import SVG from './SVG';

interface IMArkerProps {
    message?: string;
}

const svgDone = 'svgicon--done';
const svgInfo = 'svgicon--info';

export const MarkerDone = (props: IMArkerProps) => {
    return (
        <div className="maker marker--done">
          <span className="maker__text">
              {props.message || 'Done'}
          </span>
            <span className="icon icon-g maker__marker">
            <SVG xlink={svgDone} />
          </span>
        </div>
    );
};

export const MakrerException = (props: IMArkerProps) => {
    return (
        <div className="maker maker--exception">
            <span className="maker__text">
                {props.message || 'Failed!'}
            </span>
            <span className="icon icon-g maker__marker">
                <SVG xlink={svgInfo} />
            </span>
        </div>
    );
};
